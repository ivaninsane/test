import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.jbehave.core.annotations.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static org.junit.Assert.assertEquals;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfAllElementsLocatedBy;

/**
 * @author Ivan Miroshnichenko
 */
public class Steps {
    private String firstItemId;
    private WebDriver driver;
    private JavascriptExecutor jse;

    @BeforeStories
    public void init() {
        driver = new ChromeDriver();
        jse = (JavascriptExecutor) driver;
    }

    @Given("1. Открыть браузер и развернуть на весь экран.")
    public void browserOpenWithFullScreen(){
        driver.manage().window().maximize();
    }

    @When("2. Зайти на yandex.ru.")
    public void getYaRu() {
        driver.get("https://yandex.ru");
    }

    @When("3. Перейти в яндекс маркет")
    public void getMarket() {
        driver.findElement(By.linkText("Маркет")).click();
    }

    @When("4. Выбрать раздел Электроника")
    public void selectElectronics() {
        driver.findElement(By.linkText("Электроника")).click();
    }

    @When("5. Выбрать раздел <itemType>")
    public void selectTv(@Named("itemType") String itemType) {
        driver.findElement(By.linkText(itemType)).click();
    }

    @When("6. Зайти в расширенный поиск")
    public void advancedSearch() {
        driver.findElement(By.partialLinkText("Расширенный поиск")).click();
    }

    @When("7. Задать параметр поиска от <fromAmount> рублей.")
    public void setMinPrice(@Named("fromAmount") String fromAmount) {
        driver.findElements(By.className("input__control")).get(1).sendKeys(fromAmount);
    }

    @When("8. Выбрать производителей <brand>")
    public void selectBrand(@Named("brand") String brand) {
        List<String> brands = Arrays.asList(brand.split(","));
        WebElement e = driver.findElements(By.className("filter-block")).get(2);
        brands.forEach(br -> {
                    e.findElements(By.className("checkbox__label")).stream()
                            .filter(webElement ->
                                    webElement.getText().equals(br))
                            .findAny()
                            .orElseThrow((Supplier<RuntimeException>) () ->
                                    new ElementNotFoundException("Применить", "classname", "checkbox__label"))
                            .click();
                });
    }

    @When("9. Нажать кнопку Применить.")
    public void apply() throws InterruptedException {
        WebElement e = driver.findElement(By.className("filter-panel-aside__apply"));
        scrollIntoView(e);
        e.findElement(By.className("button")).click();
        Thread.sleep(3000);
    }

    @Then("10. Проверить, что элементов на странице 10.")
    public void numberOfItems() {
        List<WebElement> cardList = new WebDriverWait(driver, 10).until(presenceOfAllElementsLocatedBy(By.className("snippet-card")));
        assertEquals(10, cardList.size());
    }

    @When("11. Запомнить первый элемент в списке.")
    public void getFirstItem() {
        List<WebElement> cardList = new WebDriverWait(driver, 10).until(presenceOfAllElementsLocatedBy(By.className("snippet-card")));
        WebElement first = cardList.get(0);
        firstItemId = first.findElement(By.className("snippet-card__header-text")).getText();
    }

    @When("12. В поисковую строку ввести запомненное значение.")
    public void searchFirstItemId() throws InterruptedException {
        WebElement searchField = driver.findElement(By.id("header-search"));
        scrollIntoView(searchField);
        searchField.sendKeys(firstItemId);
    }

    @Then("13. Найти и проверить, что наименование товара соответствует запомненному значению.")
    public void checkItemIdentity() throws InterruptedException {
        driver.findElement(By.id("header-search")).submit();
        Thread.sleep(3000);
        WebElement e = driver.findElement(By.className("headline__header")).findElement(By.tagName("h1"));
        assertEquals(firstItemId, e.getText());
    }

    private void scrollIntoView(WebElement e) {
        jse.executeScript("arguments[0].scrollIntoView(true);", e);
    }
}
