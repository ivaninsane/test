Meta:

Narrative:

Scenario: first
Given 1. Открыть браузер и развернуть на весь экран.
When 2. Зайти на yandex.ru.
When 3. Перейти в яндекс маркет
When 4. Выбрать раздел Электроника
When 5. Выбрать раздел <itemType>
When 6. Зайти в расширенный поиск
When 7. Задать параметр поиска от <fromAmount> рублей.
When 8. Выбрать производителей <brand>
When 9. Нажать кнопку Применить.
Then 10. Проверить, что элементов на странице 10.
When 11. Запомнить первый элемент в списке.
When 12. В поисковую строку ввести запомненное значение.
Then 13. Найти и проверить, что наименование товара соответствует запомненному значению.

Examples:
|itemType|fromAmount|brand|
|Телевизоры|20000|Samsung,LG|
|Наушники|5000|Beats|
